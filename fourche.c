#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
void testPID1();
void testPID2();
void testPID3();
void testPID4();
void showPID();

int main(int argc, char** argv)
{
	//printf("== TEST PID 1 ==\n");
	//testPID1();

	//printf("== TEST PID 2 ==\n");
	//testPID2();
	
	printf("== TEST PID 3 ==\n");
	testPID3();
	
	//printf("== TEST PID 4 ==\n");
	//testPID4();

    return 0;
}

// Dédoublement du processus et affichage des pids
void testPID1(){
	//Affichage du pid parent
	showPID();
    	//Séparation des processus
	fork();	
	//Affichage du pid
	showPID();
}

//Dédoublement avec détection de l'enfant/parent
void testPID2(){
	showPID();
	    
    	//Séparation des processus
	int parent = fork();
	long pid = getpid();

	if(parent){
		    printf("Bonjour je suis ton père. PID ='%ld'\n",pid);
	} else {
		    printf("Nooooooooooooooooooooooo. PID ='%ld'\n",pid);
	}
	
	// Affichage des PID respectifs
	showPID();
}

// Test des 'A' 
void testPID3(){
	
	printf("== PIDs avant fork : ==\n");
	showPID();
	
	//Dédoublage du premier processus
	fork();
	
	printf("== PID après fork : ==\n");
	showPID();
	
	printf("== Entrée dans le IF ==\n");
	if(fork()){ // Dédoublage de chaque processus
		//On a pas eu 0, donc on est dans le parent
		printf("== PID dans IF (parent) : ==\n");
		showPID(); 
		
		printf("== Fork dans le if ==");
		fork(); // Dédoublage d'un des enfant
		
		printf("== PID dans IF, après fork : ==\n");
		showPID();
	}
	
	printf("A\n");
}

void testPID4(){
	//Illustration de la fonction execl
	printf("A\n");
	//Chemin complet vers executable, argument de la ligne de commande séparés par ',', pointeur NULL
	execl("/bin/ls", "ls", "-l", ".", NULL);
	printf("A\n");
} 	

void showPID(){
	long pid = getpid();
	printf("PID = '%ld'\n",pid);
}
