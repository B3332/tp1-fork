#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

int main(int argc, char** argv)
{
	// On récupère les arguments utiles
    long pid = getpid();
    int i;
    int limite = atoi(argv[1]);
    
    //Avertissement pre-itérations
    printf("%ld: debut avec itérations = %d \n", pid, limite);
    
    //Iterations pour mesurer le temps
    for(i = 0 ; i < limite ; i++){
		printf("%ld: %d \n", pid, limite-i);
		sleep(1);
	}

    //Avertissement fin intérations
    printf("%ld: fin avec %d itérations\n", pid, limite);
	
    return 0;
}
