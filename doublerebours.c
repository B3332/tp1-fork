#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

void test1();
void test2();

int main(int argc, char** argv)
{        
	//test1();
	test2();
	return 0;
}

void test1(){

	//Séparation des processus
	int p = fork(); // Attention, il faut sortir le forck du if
	
	if(p){
			execl("rebours", "rebours", "10", NULL);
	} else {
			execl("rebours", "rebours", "20", NULL);
	}
}

void test2(){
	
	int p = fork();
	int status;
	
	if(p){
		execl("rebours","rebours","10",NULL);
		waitpid(-1,&status,0);
	} else {
		execl("rebours","rebours","20",NULL);
	}
}

//pid_t wait(int *status);
//pid_t waitpid(pid_t pid, int *status, int options);

/*
    < -1 : Wait for any child process whose process group ID is equal to the absolute value of pid.
    -1 : Wait for any child process.
    0 : Wait for any child process whose process group ID is equal to that of the calling process.
    > 0 : Wait for the child whose process ID is equal to the value of pid.
*/
