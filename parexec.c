#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
void testparaexe1(int argc, char** argv);
void testparaexe2(int argc, char** argv);
void testparaexe3(int argc, char** argv);
void showPID();

int main(int argc, char** argv)
{
	//testparaexe1()
    //testparaexe2(argc,argv);
        testparaexe3(argc,argv);

    return 0;
}

//Premier test, probablement invalide
void testparaexe1(int argc, char** argv){
	
	//On montre le pid 
	showPID();
	
	//Quelques variables utiles
    int parent;
    int enCoursExec;
    //long PID[argc];
    
	for(int i = 2 ; i < argc ; i++){
		parent = fork();
		// wait(NULL); // Execution séquentielle
		if(parent){
			//PID[filled++]=p;
			enCoursExec++;
		} else {
			execl(argv[1], argv[1], argv[i], NULL);
			break;
		}
	}
	
	for(int j=0;j<enCoursExec;j++){
		wait(NULL); // Wait PID ? 
	}
}

//Deuxième test à modifier
void testparaexe2(int argc, char** argv){
	
	//On montre le pid 
	showPID();
	
	//Affichage de la commande courante
	printf("Vous avez lancé la commande: '%s'\n",argv[0]);
 
    int parent;
    int* nbProcess = (int*)malloc(sizeof(int)); //on a une variable globale en mémoire
	*nbProcess = 0;
	
	//On montre plusieurs infos
	showPID();
	printf("Le nombre de processus fils actuel est : %d \n",*nbProcess);
	printf("Le nombre maximal de processus est : %d \n",atoi(argv[2]));

	//Pour chaque argument = pour chaque commande ... 
	for(int i = 3 ; i < argc ; i++){ //On saute les deux premiers arguments
		//On fork le parent pour créer un fils :
		parent = fork();
		printf("Le parent est forké = on créé un fils.\n");
		
		if(parent){ //Si on est dans le parent
			
			printf("Le parent compare %d et %d.\n", *nbProcess, atoi(argv[2]));

			//On regarde si on a atteint le nombre max de 
			if(*nbProcess == atoi(argv[2])){
				wait(NULL);
				(*nbProcess)--;
			}
			
		} else { //Si on est dans le fils
			printf("L'enfant incrémente le nombre de process de %d à %d.\n", (*nbProcess), (*nbProcess)+1);
			(*nbProcess) = (*nbProcess) +1;
			execl(argv[1], argv[1], argv[i], NULL);
		}
	}
}


//Troisième test
void testparaexe3(int argc, char** argv){
	
	//On montre le pid 
	showPID();
	
	//Affichage de la commande courante
	printf("Vous avez lancé la commande: '%s'\n",argv[0]);
 
    int parent;
    int status;
    int* nbProcess = (int*)malloc(sizeof(int)); //on a une variable globale en mémoire
	*nbProcess = 0;
	
	//On montre plusieurs infos
	showPID();
	printf("Le nombre de processus fils actuel est : %d \n",*nbProcess);
	printf("Le nombre maximal de processus est : %d \n",atoi(argv[2]));

	//Pour chaque argument = pour chaque commande ... 
	for(int i = 3 ; i < argc ; i++){ //On saute les deux premiers arguments

		//On incrémente le nombre de fils
		printf("Le parent incrémente le nombre de process de %d à %d.\n", (*nbProcess), (*nbProcess)+1);
		(*nbProcess) = (*nbProcess) +1;
		showPID();

		//On fork le parent pour créer un fils :
		parent = fork();
		printf("Le parent est forké = on créé un fils.\n");

		if(parent){ //Si on est dans le parent
			
			printf("Le parent compare %d et %d.\n", *nbProcess, atoi(argv[2]));
			if(*nbProcess == atoi(argv[2])){//On regarde si on a atteint le nombre max de 
				//wait(NULL); // Un wait un peu plus détaillé, pour être sûr qu'il attende un seul des enfants, au hasard.
				waitpid(-1,&status,0);
				
				//Gestion du cas où un processus "fini mal"
				if(status != 0){
					printf("ERROR = status N°%d. Aborting ...",status);
					break;
				}
				
				//Un des enfants est terminé. On décrémente.
				printf("Le parent décrémente le nombre de process de %d à %d.\n", (*nbProcess), (*nbProcess)-1);
				(*nbProcess) = (*nbProcess) -1 ;
			}

		} else { //Si on est dans le fils
			//On execute, et on ne peut plus rien faire.
			execl(argv[1], argv[1], argv[i], NULL);
		}
	}
	
	//Boucle finale d'attente
	while(*nbProcess != 0){
			printf("%d processus encore en execution ... attente.\n", (*nbProcess));
			waitpid(-1,&status,0);
			(*nbProcess) = (*nbProcess) -1 ;
	}
}

void showPID(){
	long pid = getpid();
	printf("PID = '%ld'\n",pid);
}
